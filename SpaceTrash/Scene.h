#ifndef SCENE_H
#define SCENE_H

#define BOT 0x00
#define ASREROID 0x01


#include<SDL2\SDL.h>
#include<SDL2\SDL_image.h>
#include<iostream>
#include<string>
#include"Display.h"
#include"Sound.h"
#include"Weapon.h"
#include"Bot.h"
#include"Asteroid.h"
#include"Ship.h"

typedef struct BACKGROUND_TYP 
{
	SDL_Texture* texture;
	SDL_Rect rectW;
}BGWORLD;

class Scene
{

private:

	int m_worldWidth; //������� ����
	int m_worldHeight;

	float time;// ������� ���������
	BGWORLD m_bgworld;

	Sound* m_sound;
	SDL_Renderer* m_render;
	SDL_Texture* m_asset;
	//������� ������
	std::list<Bullet*> lbullet;	//������ ��������
	std::list<Bullet*>::iterator itBullets;//�������� ��� ����

	std::list<Bullet*> labullet;	//������ ��������
	std::list<Bullet*>::iterator itaBullets;//�������� ��� ����
										  
	std::list<Bullet*> enemyBullet;	 //������ �������� �����������
	std::list<Bullet*>::iterator itEnemyBullets;//�������� ��� ����
												
	std::list<Asteroid*> lAstr; //������ ���������
	std::list<Asteroid*>::iterator litAstr;//�������� ��� ����������
										   //enemy
	std::list<Bot*> lBot;  //������ ��
	std::list<Bot*>::iterator itvBot;//������� ��� �����

public:

	Scene(Sound* _sound,const std::string& file ,SDL_Renderer* _render) :m_worldWidth(1000), m_worldHeight(768)
	{
		m_sound = _sound;
		m_render = _render;
		m_bgworld.rectW = { 0,0,m_worldWidth,m_worldHeight };
		
		m_bgworld.texture = SDL_CreateTextureFromSurface(m_render, 
			Image::LoadImage(file.c_str()));//Image::LoadImade(file.c_str(),m_render);
	}

	void SetAsset(SDL_Texture* asset) { m_asset = asset; }

	void isLoop(float time, Entity* obj)
	{
		//������� ������� �������� ������� ����
		for (litAstr = lAstr.begin(); litAstr != lAstr.end(); ++litAstr)
			CheckOutAbroad(*litAstr);

		//for (itvBot = lBot.begin(); itvBot != lBot.end(); ++itvBot)
		//{
		////	(*itvBot)->setSizeWorld(m_bgworld.rectW.w, m_bgworld.rectW.h);
		//}

		//CheckOutAbroad(obj);
		//�������� ��. ���� ����� ���������� � ������� ����������� ��, �� �� ������� ������, �����
		//�� ���������� ������������� �� ������ ��������.
		for (itvBot = lBot.begin(); itvBot != lBot.end(); ++itvBot)
		{
			(*itvBot)->Update(time);
			float dist = sqrt((obj->GetPosition().x - (*itvBot)->GetPosition().x)*(obj->GetPosition().x - (*itvBot)->GetPosition().x) +
				(obj->GetPosition().y - (*itvBot)->GetPosition().y)*(obj->GetPosition().y - (*itvBot)->GetPosition().y));

			if (dist < 150)
			{
				m_sound->PlayEffect();
				(*itvBot)->SetTargetXY(obj->GetPosition());
				enemyBullet.splice(enemyBullet.end(), (*itvBot)->Shot());
				(*itvBot)->Move();
			}
			else
			{
				(*itvBot)->SetAcceleration(8.3f);
				(*itvBot)->FollowOfPath();
				(*itvBot)->Move();
			}
		}

		//
		////------------------------------------------------------------------
		////�������� ����	 
		////---------------������
		for (itBullets = labullet.begin(); itBullets != labullet.end(); ++itBullets)
		{
			(*itBullets)->SetSpriteDate(m_asset, { 374,0,11,31 });
			lbullet.push_back(*itBullets);
		}

		labullet.clear();;
		
		for (itaBullets = lbullet.begin(); itaBullets != lbullet.end(); ++itaBullets)
		{
			(*itaBullets)->SetSpriteDate(m_asset, { 374,0,11,31 });
			(*itaBullets)->Update(time);
			(*itaBullets)->Move();
		}
		//------------- ������
		for (itEnemyBullets = enemyBullet.begin(); itEnemyBullets != enemyBullet.end(); ++itEnemyBullets)
		{
			(*itEnemyBullets)->SetSpriteDate(m_asset, { 374,31,9,29 });
			(*itEnemyBullets)->Update(time);
			(*itEnemyBullets)->Move();

		//	std::cout << enemyBullet.size() << std::endl;
		}
		//------------------------------------------------------------------
		for (litAstr = lAstr.begin(); litAstr != lAstr.end(); ++litAstr)
		{
			(*litAstr)->Update(time);
			(*litAstr)->Move();
		}
		//�������� ������������ ���� � ������, ���� ������������� ���������, �� ���� ����������
		for (itvBot = lBot.begin(); itvBot != lBot.end(); ++itvBot)
		{
			for (itBullets = lbullet.begin(); itBullets != lbullet.end();)
			{
				if (SDL_HasIntersection(&(*itBullets)->GetSpriteData()->positionRect,
										&(*itvBot)->GetSpriteData()->positionRect))
				{
					
					(*itvBot)->SetDamage(-(*itBullets)->getDamage());
					delete (*itBullets);
					itBullets = lbullet.erase(itBullets);
				}
				else itBullets++;
			}

		}
		//�������� ������� (�����/�������) �������. � ������ ������ ������
		//���� ����� �������, ���� �� ��������������� � ���. ������������ ��� �������
		//if (obj->getCurrentHP() <= 0)
		//	obj->setLife(false);
		//else
		//{	

		for (itEnemyBullets = enemyBullet.begin(); itEnemyBullets != enemyBullet.end();)
		{
			if (SDL_HasIntersection(&obj->GetSpriteData()->positionRect,
				&(*itEnemyBullets)->GetSpriteData()->positionRect))
			{
				obj->SetDamage(-(*itEnemyBullets)->getDamage());
				delete (*itEnemyBullets);
				itEnemyBullets = enemyBullet.erase(itEnemyBullets);
			}
			else itEnemyBullets++;
		}

		//}
		//�������� ������������ ���������� � �����
		for (auto liAstr = lAstr.begin(); liAstr != lAstr.end(); ++liAstr)
			for (litAstr = lAstr.begin(); litAstr != lAstr.end(); ++litAstr)
			{
				if (liAstr != litAstr)
				{
					if (SDL_HasIntersection(&(*liAstr)->GetSpriteData()->positionRect,
						&(*litAstr)->GetSpriteData()->positionRect))
					{
						(*liAstr)->SetVelocity(Vec2((*liAstr)->GetVelocity().x * -1, (*liAstr)->GetVelocity().y*-1));
						(*litAstr)->SetVelocity(Vec2((*litAstr)->GetVelocity().x * -1, (*litAstr)->GetVelocity().y*-1));
					}

				}

			}

		for (litAstr = lAstr.begin(); litAstr != lAstr.end(); ++litAstr)
			for (itBullets = lbullet.begin(); itBullets != lbullet.end();)
			{
				if (SDL_HasIntersection(&(*litAstr)->GetSpriteData()->positionRect,
					&(*itBullets)->GetSpriteData()->positionRect))
				{
					(*litAstr)->SetDamage(-(*itBullets)->getDamage());
					delete (*itBullets);
					itBullets = lbullet.erase(itBullets);

				}
				else itBullets++;
			}
	
	}


	void isRender(Display* display) 
	{
		SDL_RenderCopy(m_render, m_bgworld.texture, nullptr, &m_bgworld.rectW);
		////��������� 
		////---------����
		////--------------������
		for (itaBullets = lbullet.begin(); itaBullets != lbullet.end(); ++itaBullets)
		{
			display->Render(*itaBullets);
		}
		//--------------������
		for (itEnemyBullets = enemyBullet.begin(); itEnemyBullets != enemyBullet.end(); ++itEnemyBullets)
			display->Render(*itEnemyBullets);
		//----------����������
		for (litAstr = lAstr.begin(); litAstr != lAstr.end(); ++litAstr)
			display->Render(*litAstr);

		//---------��
		for (itvBot = lBot.begin(); itvBot != lBot.end(); ++itvBot)
			display->Render(*itvBot);
	
	//	std::cout << enemyBullet.size() << std::endl;
	}

	//For Adds bots ADD_BOT
	//For Adds asteroid ADD_ASTEROID 
	void CreateEnvironment(int flags, SDL_Texture* assets)
	{
		switch (flags)
		{
		
			case BOT: 
			{
				Bot* temp = new Bot(Vec2(static_cast<float>(m_worldWidth - rand() % m_worldWidth * 2),
					static_cast<float>(m_worldHeight - rand() % m_worldHeight * 2)), 1.f);
				temp->SetSpriteDate(assets, { 15,100,64,99 });
				lBot.push_back(temp);
			}break; 

			case ASREROID: 
			{
				Asteroid* temp = new Asteroid(Vec2(static_cast<float>(m_worldWidth - rand() % m_worldWidth * 2),
					static_cast<float>(m_worldHeight - rand() % m_worldHeight * 2)),
					static_cast<float>(2 + rand() % 3), static_cast<float>(1 - rand() % 2));

				temp->SetSpriteDate(assets, { 3,204,55,53 });
				lAstr.push_back(temp);
			} break;
		}
	}
	void setBullet(std::list<Bullet*>& lbul) 
	{
		labullet.splice(labullet.end(), lbul);
	}

	void isClearScene() 
	{
		/*for (litAstr = lAstr.begin(); litAstr != lAstr.end(); ++litAstr)
			for (itBullets = lbullet.begin(); itBullets != lbullet.end();)
			{
				if (SDL_HasIntersection(&(*litAstr)->GetSpriteData()->positionRect,
										&(*itBullets)->GetSpriteData()->positionRect))
				{
					(*litAstr)->SetDamage(-(*itBullets)->getDamage());
					delete (*itBullets);
					itBullets = lbullet.erase(itBullets);

				}
				else itBullets++;
			}*/
		  
		if (!lBot.empty()) 
		{
			for (itvBot = lBot.begin(); itvBot != lBot.end();)
			{
				if ((*itvBot)->GetCurrentHP() <= 0.0f)
				{
					delete (*itvBot);
					itvBot = lBot.erase(itvBot);
				}
				else itvBot++;
			}
		}
		
		if (!lAstr.empty())
		{
			for (litAstr = lAstr.begin(); litAstr != lAstr.end();)
			{
				Asteroid* ast = (*litAstr);							   
				if ((*litAstr)->GetCurrentHP() <= 0.0f)
				{
					litAstr = lAstr.erase(litAstr);
				//	lAstr.splice(lAstr.begin(), ast->GetBody()->Death());
					delete ast;
				}
				else litAstr++;
			}
		}
		
		if (!lbullet.empty())
			for (itBullets = lbullet.begin(); itBullets != lbullet.end();)
			{
				if (((*itBullets)->GetPosition().x >= m_worldWidth) || ((*itBullets)->GetPosition().y >= m_worldHeight) ||
					((*itBullets)->GetPosition().x <= -m_worldWidth) || ((*itBullets)->GetPosition().y <= -m_worldHeight))
				{
					delete(*itBullets);
					itBullets = lbullet.erase(itBullets);
				}
				else ++itBullets;
			}

		if (!enemyBullet.empty())
			for (itEnemyBullets = enemyBullet.begin(); itEnemyBullets != enemyBullet.end();)
			{
				if (((*itEnemyBullets)->GetPosition().x >= m_worldWidth) || ((*itEnemyBullets)->GetPosition().y >= m_worldHeight) ||
					((*itEnemyBullets)->GetPosition().x <= -m_worldWidth) || ((*itEnemyBullets)->GetPosition().y <= -m_worldHeight))
				{
					delete(*itEnemyBullets);
					itEnemyBullets = enemyBullet.erase(itEnemyBullets);
				}
				else ++itEnemyBullets;

			}
	}	 

	void isDestroy(int flags) 
	{
		switch (flags)
		{
			case BOT:
				if (!lBot.empty())
				{
					for (itvBot = lBot.begin(); itvBot != lBot.end(); ++itvBot)
						delete (*itvBot);
					lBot.clear();
				}
			break;	

			case ASREROID: 
				{
					if (!lAstr.empty())
					{
						for (litAstr = lAstr.begin(); litAstr != lAstr.end(); ++litAstr)
							delete (*litAstr);
						lAstr.clear();
					}
				}
			break;
		}
				
	}

	Vec2* getSizes() { return &Vec2(static_cast<float>(m_bgworld.rectW.w),
									static_cast<float>(m_bgworld.rectW.h)); }
	virtual ~Scene()
	{	
		if (!lBot.empty())
		{
			for (itvBot = lBot.begin(); itvBot != lBot.end(); ++itvBot)
				delete (*itvBot);
			lBot.clear();
		}
		 
		if (!lAstr.empty())
		{
			for (litAstr = lAstr.begin(); litAstr != lAstr.end(); ++litAstr)
				delete (*litAstr);
			lAstr.clear();
		}
			 
		if (!enemyBullet.empty())
		{
			for (itEnemyBullets = enemyBullet.begin(); itEnemyBullets != enemyBullet.end(); ++itEnemyBullets)
				delete (*itEnemyBullets);
			enemyBullet.clear();
		}																						

		if (!lbullet.empty())
		{
			for (itBullets = lbullet.begin(); itBullets != lbullet.end(); itBullets++)
				delete (*itBullets);
			lbullet.clear();
		}

		SDL_DestroyTexture(m_bgworld.texture);
		m_bgworld.texture = nullptr;
	}

	void CheckOutAbroad(Object* obj)
	{
		if (obj->GetPosition().x >  static_cast<float>(m_worldWidth))
				obj->SetPosition(Vec2(0.f, obj->GetPosition().y));
		if (obj->GetPosition().x < 0.f)
			obj->SetPosition(Vec2(static_cast<float>(m_worldWidth), 
			obj->GetPosition().y));
		if (obj->GetPosition().y > static_cast<float>(m_worldHeight))
				obj->SetPosition(Vec2(obj->GetPosition().x, 0.f));
		if (obj->GetPosition().y < 0.f)	obj->SetPosition(Vec2(obj->GetPosition().x,
														static_cast<float>(m_worldHeight)));
	}
};

#endif // !SCENE_H