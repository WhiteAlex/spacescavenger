
#pragma once
#include "Entity.h"
#include <cstdlib>
#include "NormWeapom.h"

class CShip:
	public Entity
{

	
public:
	CShip(Vec2 _xy);
	void Transfer(Vec2 _transfer);
	void Update(float t);
	bool getShot() { return isShot; }
//	Vec2 getW1Position() { return w1->getPosition(); }
//	Vec2 getW2Position() { return w2->getPosition(); }
//	void SetWeapon1(Weapon* weapon) { w1 = weapon; w1->setAmmo(30);}
//	void SetWeapon2(Weapon* weapon) { w2 = weapon; }

	std::list<Bullet*> Shot();
	void SetTargetXY(Vec2 & _xy);
	~CShip();
private:
	CShip(const CShip& cship);
	TimeShot ts; // ����� ����� ����������
	bool isShot; // ���������� � �������
	float tx; //���������� �� �
	float ty; //���������� �� �
	Vec2 stoping;
	Vec2 impuls;
	float maxVelocity; //������������ ���������
	NormWeapon* nw1;

};

