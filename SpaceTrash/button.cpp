#include "button.h"



button::button(SDL_Renderer * _render, const std::string & file, const SDL_Rect _rect, const std::string & text):
caption(text)
{
	m_rect.x = _rect.x;
	m_rect.y = _rect.y;
	m_rect.w = _rect.w;
	m_rect.h = _rect.h;

	backgroundButton = Image::LoadImade(file,_render);

}


button * button::Create(SDL_Renderer * _render, const std::string & file, const SDL_Rect _rect, const std::string & text)
{
	return &button(_render,file,_rect,text);
}

void button::Show()
{
	SDL_RenderCopy(m_render, backgroundButton, nullptr, &m_rect);
	///SDL_RenderCopy
}

button::~button()
{
}
