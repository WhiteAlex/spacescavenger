#pragma once

#include<iostream>
#include<SDL2\SDL_image.h>
#include<SDL2\SDL_ttf.h>
#include"Scene.h"
#include"Display.h"
#include"Text.h"
#include"Timer.h"

class Game
{
	
private:
	Display* m_display;
	Sound* m_sound;
	Scene* m_scene;

	bool isRun;
	SDL_Event sdl_event;
	MYFPS fps = { 0,0,0,0 };
	MYTIME m_time = { 0,0,0,0,0 };
	SDL_Rect rectP;
	SDL_Texture* assets;
	// ������� ������
	int screen_width;
	int screen_height;
	//��������� ����. �������� ����� ������� ���-�� ���������
	Vec2 mouse;
	Text* text;


	// ������� �������
    CShip *pship;

	SDL_Joystick* joystick;

	float time;// ������� ���������
	const Uint8 *keystate;

	SDL_Surface* cur;
	SDL_Cursor* cursor;


private:
	int SetCursorPosition(float x, float y);

public:
	Game();
	void isLoop();
	void isEvent(SDL_Event *e);
	void Render();
	void Cleanup();
	bool isExecute();
	void GUI_Render();
	~Game();
};