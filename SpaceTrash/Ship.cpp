#include "Ship.h"

CShip::CShip(Vec2 _xy):
			Entity(_xy,1),nw1(nullptr),maxVelocity(5)
{
	//���������� ��������� ������
	nw1 = new NormWeapon(m_position + Vec2(static_cast<float>(m_spriteDate.spriteRect.w / 2),
		static_cast<float>(m_spriteDate.spriteRect.h / 2)), 4.f);
}
  //����������� 
void CShip::Transfer(Vec2 _transfer)
{
	stoping.x += _transfer.x;
	stoping.y += _transfer.y;

	if (stoping.x >= maxVelocity) stoping.x = maxVelocity;
		else
	if (stoping.x <= -maxVelocity) stoping.x = -maxVelocity;

	if (stoping.y >= maxVelocity) stoping.y = maxVelocity;
		else
	if (stoping.y <= -maxVelocity) stoping.y = -maxVelocity;

}
void CShip::Update(float t)
{
	time = t;
	dt += t;
	nw1->Update();
	//������������ 
	m_position.x += impuls.x;
	m_position.y += impuls.y;

	m_pctHP = m_maxHP / 100;
	m_currentPctHP = m_currentHP / m_pctHP;
	

	/*if (m_spriteDate.spriteRect.x < 370)
		m_spriteDate.spriteRect.x += 30*dt;
	else
		m_spriteDate.spriteRect.x = 15;*/

	impuls = Vec2(stoping.x -= stoping.x / dt, stoping.y -= stoping.y / dt);
	m_spriteDate.positionRect.x = static_cast<int>(m_position.x);
	m_spriteDate.positionRect.y = static_cast<int>(m_position.y);

	ts.currentTimeShot = (float)SDL_GetTicks();
	//�������� ������������� ���������� � �������
	//if (ts.currentTimeShot - ts.prevTimeShot >= ts.cooldown)
	//	isShot = false;
	m_spriteDate.angle = (atan2(m_target.x - (m_position.x + m_spriteDate.spriteRect.w / 2),
								m_target.y - (m_position.y + m_spriteDate.spriteRect.h / 2)) * 180 / PI);


	nw1->setXY(m_position + Vec2(static_cast<float>(m_spriteDate.spriteRect.w / 2),
								static_cast<float>(m_spriteDate.spriteRect.h / 2)), m_spriteDate.angle);
}

CShip::~CShip()
{
	delete nw1;
}

std::list<Bullet*> CShip::Shot()
{
	return nw1->Shot(m_target);;
}

void CShip::SetTargetXY(Vec2& _xy) {
	m_target = _xy;
}