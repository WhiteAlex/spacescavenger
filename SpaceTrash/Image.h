#ifndef IMAGE_H
#define	 IMAGE_H
#include<SDL2\SDL.h>
#include<SDL2\SDL_image.h>

#include<string>
class Image
{
	Image();
public:

	static SDL_Surface * LoadImage(const std::string& file);
	static SDL_Surface * LoadImage(const std::string & file, Uint8 r, Uint8 g, Uint8 b);
	static SDL_Surface * LoadImageAsSurface(const std::string& file, int posX, int posY, int width, int height);
	static SDL_Texture * LoadImade(const std::string & file, SDL_Renderer * ren, int posX, int posY, int width, int height);
    static SDL_Texture * LoadImade(const std::string& file, SDL_Renderer* ren);

	virtual ~Image();
};
#endif //!IMAGE_H