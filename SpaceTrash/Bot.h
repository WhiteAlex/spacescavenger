#pragma once
#include "Entity.h"
#include "Weapon.h"
#include <list>


class Bot :
	public Entity
{


public:
	Bot(Vec2 _xy, float zs);
	void Move();
	void Update(float t);
	void FollowOfPath();
	bool getReadeyToShot();
	std::list<Bullet*> Shot();
	virtual ~Bot();
private:
	int currentPoint;//������� �������
	int countCheckPoints;//���-�� ����������� �����
	VERTEX2D *route; //���������� ����������� ����� 

	Weapon* w1;

	float tx;
	float ty;
	float d;
	TimeShot ts;
	bool isShot;

	//������� ����	 //�������
	int nWidWin; //������ ����
	int nHeigWIn; //������ ����

private:
	void SetPointRoute(float x, float y);
	Bot(const Bot& bot);

};

