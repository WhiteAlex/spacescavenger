#include "Bot.h"

Bot::Bot(Vec2 _xy, float zs) : Entity(_xy, zs),
countCheckPoints(1 + rand() % 8), currentPoint(0), w1(nullptr),
ts({0,0,1500}),nWidWin(1000),nHeigWIn(1000)
{
	
//	Vec2 m_pos = m_position + Vec2(m_spriteDate.spriteRect.w / 2, m_spriteDate.spriteRect.h / 2);

	w1 = new Weapon(m_position,1,2);
	route = new VERTEX2D[countCheckPoints]; // ���� �� �������� ������� ����

	for (int i = 0; i < countCheckPoints; ++i)// �������� ���� � ������� ��������� ���������
	{
		route[i].x = static_cast<float>(-nWidWin + rand() % nWidWin*2);
		route[i].y = static_cast<float>(-nHeigWIn + rand() % nHeigWIn*2);
	}

	m_target = route[0];

	 tx = m_target.x - m_position.x;
	 ty = m_target.y - m_position.y;
	 
	 d = sqrt(tx*tx + ty*ty);

	 m_acceleration = 8.3f;
}
void Bot::Move()
{
	 tx = m_target.x - m_position.x;
	 ty = m_target.y - m_position.y;
	 d = sqrt(tx*tx + ty*ty);

	 m_velocity = Vec2(tx/d, ty/d);
	
	 m_position.x += m_acceleration * m_velocity.x*time;
	 m_position.y += m_acceleration * m_velocity.y*time;
}

void Bot::SetPointRoute(float x, float y)
{
	 d = sqrt(tx*tx + ty*ty);
	if (d >= -10 && d<=50) currentPoint++;
	SetTargetXY(Vec2(route[currentPoint].x, route[currentPoint].y));
}

void Bot::FollowOfPath()
{
	SetPointRoute(route[currentPoint].x, route[currentPoint].y);
	if (currentPoint == countCheckPoints)
	{
		delete[] route;
		
		countCheckPoints = 1+rand() % 8;
		route = new VERTEX2D[countCheckPoints];

		for (int i = 0; i < countCheckPoints; ++i)
		{
			route[i].x = static_cast<float>(-nWidWin + rand() % nWidWin*2);
			route[i].y = static_cast<float>(-nHeigWIn + rand() % nHeigWIn*2);
		}
		currentPoint = 0;
	}
}


Bot::~Bot()
{
//	std::cout << "\nEnemy destroy\n";
}

std::list<Bullet*> Bot::Shot()
{		
	w1->setXY(Vec2(m_position.x + m_spriteDate.spriteRect.w / 2,
		m_position.y + m_spriteDate.spriteRect.h / 2));
	return  w1->Shot(m_target);
}

bool Bot::getReadeyToShot() {
	return isShot;
}

void Bot::Update(float t)
{	
	time = t;
	m_pctHP = m_maxHP / 100;
	m_currentPctHP = m_currentHP / m_pctHP;
	w1->Update();

	//���������� ���� ��� ��������

	float tx = m_target.x - m_position.x;
	float ty = m_target.y - m_position.y;

	m_spriteDate.angle = (atan2(tx, ty) * 180 / PI);

//	Vec2 m_pos = Vec2(m_spriteDate.spriteRect.w / 2, m_spriteDate.spriteRect.h / 2);

	double ang = m_spriteDate.angle * PI / 180;
	double xr = 0, yr = 0;
	xr = 0 * cos(-ang) - 10 * sin(-ang);
	yr = 0 * sin(-ang) + 10 * cos(-ang);
//	Vec2 m_pos = m_position + Vec2(m_spriteDate.spriteRect.w / 2, m_spriteDate.spriteRect.h / 2);
	
	m_spriteDate.positionRect.x = static_cast<int>(m_position.x);
	m_spriteDate.positionRect.y = static_cast<int>(m_position.y);

	w1->setXY(Vec2(m_position.x + m_spriteDate.spriteRect.w / 2,
					m_position.y + m_spriteDate.spriteRect.h / 2));


}