#include "Image.h"

Image::Image()
{
}

//Load full image
SDL_Surface * Image::LoadImage(const std::string & file)
{
	SDL_Surface* surface = nullptr;
	SDL_Surface* temp_surface = nullptr;

	
	if ((temp_surface = IMG_Load(file.c_str())) != nullptr)
		surface = SDL_ConvertSurfaceFormat(temp_surface,SDL_PIXELFORMAT_ABGR8888,0);

	SDL_FreeSurface(temp_surface);
	return surface;
}
 

SDL_Surface * Image::LoadImage(const std::string & file, Uint8 r, Uint8 g, Uint8 b)
{
	SDL_Surface* surface = nullptr;
	SDL_Surface* temp_surface = nullptr;


	if ((temp_surface = IMG_Load(file.c_str())) != nullptr)
		surface = SDL_ConvertSurfaceFormat(temp_surface, SDL_PIXELFORMAT_ABGR8888, 0);
	SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, r, g, b));
	SDL_FreeSurface(temp_surface);
	return surface;
}

SDL_Surface * Image::LoadImageAsSurface(const std::string & file, int posX, int posY, int width, int height)
{
	SDL_Surface* surface = nullptr;
	SDL_Surface* temp_surface = nullptr; 

	if ((temp_surface = IMG_Load(file.c_str())) != nullptr)
		surface = SDL_ConvertSurfaceFormat(temp_surface, SDL_PIXELFORMAT_ABGR8888, 0);
	SDL_FreeSurface(temp_surface);
	return surface;
}

SDL_Texture * Image::LoadImade(const std::string & file, SDL_Renderer * ren)
{
	SDL_Texture* texture = IMG_LoadTexture(ren,file.c_str());
	if(texture!=nullptr)
	return texture;	

	SDL_DestroyTexture(texture);
	return nullptr;
}

SDL_Texture * Image::LoadImade(const std::string & file, SDL_Renderer * ren, int posX, int posY, int width, int height)
{
	SDL_Texture* texture = IMG_LoadTexture(ren, file.c_str());
	
	SDL_Rect src = { posX, posY, width, height };
	//SDL_RenderGetClipRect(ren, &src);
	if (texture != nullptr)
		return texture;

	SDL_DestroyTexture(texture);
	return nullptr;
}

Image::~Image()
{
}
