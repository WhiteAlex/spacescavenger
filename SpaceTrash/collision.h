#pragma once

#include<math.h>

	typedef struct VERTEX2D_TYP
	{
		double x, y;
	}VERTEX2D, *VERTEX2D_PTR;

class collision
{

private:
	VERTEX2D operator-(VERTEX2D const a);
	VERTEX2D operator*(float s);
	float operator*(VERTEX2D const a);
	VERTEX2D operator+(VERTEX2D const a);

	collision();
	~collision();
	
public:
	//��������� ��������
	 VERTEX2D Subtraction(VERTEX2D const a, VERTEX2D const b);
	//�������� ��������
	VERTEX2D addition(VERTEX2D const a, VERTEX2D const b);

	VERTEX2D VectorOnScolar( VERTEX2D const a, float const b);

	double ScolarProductVector(VERTEX2D const a, VERTEX2D const b);
	//����������� ���������
	double getDist(VERTEX2D const a, VERTEX2D const b);
	//������������ �������
	VERTEX2D NormVector(VERTEX2D const vec);
	//���������� �������
	VERTEX2D DetermineVector(VERTEX2D const a, VERTEX2D const b);
	//����� �������
	double LengthVector(VERTEX2D const &a);
	//����������� ������������
	bool isCollision(VERTEX2D p0, VERTEX2D p1, VERTEX2D p2, VERTEX2D p3);

};

