#ifndef SOUND_H
#define SOUND_H
#include<SDL2\SDL_mixer.h>
#include<iostream>
#include<string>
class Sound
{
	Mix_Chunk* m_soundEffect;
	Mix_Music* m_backgroundMusic;
public:
	Sound();
	void PlayEffect();
	void PlayMusic();
	void StopMusic();
	virtual ~Sound();
};

#endif // !SOUND_H



