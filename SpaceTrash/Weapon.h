#ifndef WEAPON_H_INCLUDED
#define WEAPON_H_INCLUDED

#include"Entity.h"
class Bullet :
	public Entity
{
	float dist;
	float damage;

public:
	Bullet(Vec2 _xy, Vec2 _target, float dmg);
	void Move();
	float getDamage();
	//void Update(float t);
	 ~Bullet();
};

#include <list>
#include"Timer.h"
class Weapon
{

public:
	Weapon(Vec2 _xy, int count, float damage);
	Weapon(Vec2 _xy, Vec2 _target);
	//������� �������
	void setXY(Vec2 _xy);
	void setTarget(Vec2 _xy);
	Vec2 getPosition() { return owner; }
	void Update();
	std::list<Bullet*> Shot(Vec2& _tar);
	int getAmmo();
	void setAmmo(int am);
	~Weapon();

protected:
	int			  ammo;
	bool		  isShot;
	unsigned int  countBullet; //���������� ����
	Vec2		  owner; // ���������� ������
	float		  damage;
	Timer		  m_timer;
	SDL_Texture*  t;
	SDL_Renderer* r;


private:
	Weapon(const Weapon& weapon);
};
#endif WEAPON_H_INCLUDE

