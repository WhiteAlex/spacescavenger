#include "collision.h"


double collision::ScolarProductVector(VERTEX2D const a, VERTEX2D const b)
{
	return a.x*b.x + a.y*b.y;
}

//Расстояние
double collision::getDist(VERTEX2D const a, VERTEX2D const b)
{
	VERTEX2D d;
	d.x = a.x - b.x;
	d.y = a.y - b.y;
	return sqrt(d.x*d.x+d.y*d.y);
}
//Нормализованый вектор
inline VERTEX2D collision::NormVector(VERTEX2D const vec)
{
	VERTEX2D v;
	double d = LengthVector(vec);
	v.x = vec.x / d;
	v.y = vec.y / d;

	if (v.x*v.x + v.y*v.y)
	return v;
}

VERTEX2D collision::DetermineVector(VERTEX2D const a, VERTEX2D const b)
{
	VERTEX2D v;

	v.x = b.x - a.x;
	v.y = b.y - a.y;

	return v;
}

double collision::LengthVector( VERTEX2D const &a)
{
	return sqrt(a.x*a.x+a.y*a.y);
}

bool collision::isCollision(VERTEX2D p0, VERTEX2D p1, VERTEX2D p2, VERTEX2D p3)
{
	VERTEX2D U, V, S1, S2;
	double s, t;

	S1.x = p1.x - p0.x;
	S1.y = p1.y - p0.y;

	S2.x = p3.x - p2.x;
	S2.y = p3.y - p2.y;

	double d1[2][2], // det  for s
		   d2[2][2], // det  for t
		   d3[2][2]; // for all

	d1[0][0] = p0.x - p2.x;
	d1[1][0] = p0.y - p2.y;
	d1[0][1] = -S1.x;
	d1[1][1] = -S1.y;

	d2[0][0] = S2.x;
	d2[1][0] = S2.y;
	d2[0][1] = d1[0][0];
	d2[1][1] = d1[1][0];
	
	d3[0][0] = S2.x;
	d3[1][0] = S2.y;
	d3[0][1] = -S1.x;
	d3[1][1] = -S1.y;

	s = (d1[0][0] * d1[1][1] + d1[1][0] * d1[0][1]) / (d3[0][0] * d3[1][1] + d3[1][0] * d3[0][1]);
	t = (d2[0][0] * d2[1][1] + d2[1][0] * d2[0][1]) / (d3[0][0] * d3[1][1] + d3[1][0] * d3[0][1]);

	if ((s >= 0 && s <= 1) && (t >= 0 && t <= 1))
	{
		VERTEX2D temp_s1, temp_s2;

		temp_s1.x = S1.x*t;
		temp_s1.y = S1.y*t;

		temp_s2.x = S2.x*s;
		temp_s2.y = S2.y*s;

		U.x = p0.x + temp_s1.x;
		U.y = p0.y + temp_s1.y;

		V.x = p2.x + temp_s2.x;
		V.y = p2.y + temp_s2.y;

		if ((U.x==V.x)&&(U.y == V.y)) return true;

	}

	return false;
}

collision::~collision()
{
}

VERTEX2D collision::Subtraction(VERTEX2D const a, VERTEX2D const b)
{
	VERTEX2D v;

	v.x = a.x - b.x;
	v.y = a.y - b.y;

	return v;
}

VERTEX2D collision::addition(const VERTEX2D a, const VERTEX2D b)
{
	VERTEX2D v;

	v.x = a.x + b.x;
	v.y = a.y + b.y;

	return v;
}

VERTEX2D collision::VectorOnScolar(VERTEX2D const a, float const b)
{	VERTEX2D v;

	v.x = a.x * b;
	v.y = a.y * b;

	return v;
}
