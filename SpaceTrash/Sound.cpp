#include "Sound.h"



Sound::Sound()
{
  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0)
		std::cout << "Error Mix_OpenAudio: " << Mix_GetError();

	m_backgroundMusic = Mix_LoadMUS("res/Sound/b.mp3");		
	//Mix_Volume(2, 0);
	m_soundEffect = Mix_LoadWAV("res/Sound/shoot.wav");
	m_soundEffect->volume = 2;

}								  

void Sound::PlayEffect()
{
	Mix_PlayChannel(-1, m_soundEffect, 0);
}

void Sound::PlayMusic() {
	Mix_PlayMusic(m_backgroundMusic,0);
}

void Sound::StopMusic()
{
	/*if (Mix_PlayingMusic())Mix_PauseMusic();*/
}

Sound::~Sound()
{
	//StopMusic();
	Mix_FreeMusic(m_backgroundMusic);
	m_backgroundMusic = nullptr;
	Mix_FreeChunk(m_soundEffect);
	m_soundEffect = nullptr;
	Mix_Quit();
}
