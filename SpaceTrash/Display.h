#ifndef DISPLAY_H_
#define DISPLAY_H_

//#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include <SDL2\SDL.h>
#include "math.h"
#include "Object.h"

class Display
{
	SDL_Window* m_window;
	SDL_Renderer* m_render;
	Vec2 m_windowSize;

public:
	Display(std::string name, int width, int height);
	void isClear();
	void RenderPresent();

	void Render(SpriteData* const sprt) 
	{		
		SDL_RendererFlip flip = SDL_FLIP_VERTICAL;
		SDL_RenderCopyEx(m_render,
			sprt->pSprite,
			&sprt->spriteRect,
			&sprt->positionRect,
			-sprt->angle,
			&sprt->pointFlip,
			flip);
	}

	void Render(Object* obj)
	{
		SDL_RendererFlip flip = SDL_FLIP_VERTICAL;
	//	if(obj->GetSpriteData())
		SDL_RenderCopyEx(m_render,
			obj->GetSpriteData()->pSprite,
			&obj->GetSpriteData()->spriteRect,
			&obj->GetSpriteData()->positionRect,
			-obj->GetSpriteData()->angle,
			&obj->GetSpriteData()->pointFlip,
			flip);
	}
	/*
	void TextShow(const int x, const int y, const std::string text);
	void TextShow(const int x,const int y, const std::string text,const int n);
	void TextShow(const int x,const int y, const std::string text,const int n, const int n1);
	*/
	inline Vec2& getWindowSize() { return m_windowSize; }
	inline SDL_Window* getWindow() { return m_window; }
	inline SDL_Renderer* getRender() { return m_render; }
	void Event(SDL_Event *p_event);

	~Display();
};

#endif //DISPLAY_H_