#include "Sprite.h"




Sprite::Sprite(SDL_Renderer* render, const std::string& fileName, int x, int y, int width, int height):
	m_render(render)
{
	m_sprite = SDL_CreateTextureFromSurface(render, Image::LoadImageAsSurface(fileName, x, y, width, height) );
	m_rect = {x, y, width, height};
	m_center = { width / 2, height / 2 };

	m_bound.x = static_cast<float>(width / 2);
	m_bound.y = static_cast<float>(height / 2);
	m_bound.z = static_cast<float>(-width / 2);
	m_bound.w = static_cast<float>(-height / 2);

	m_data.pSprite = m_sprite;
	m_data.spriteRect = m_SpriteRect;
	m_data.postionRect = m_rect;
	m_data.size = m_size;//��������
	m_data.angle = m_angle;	
	m_data.pointFlip = m_center;

	renderFlip = SDL_FLIP_VERTICAL;
}

Sprite::Sprite(SDL_Renderer * render, SDL_Texture * sprite, SDL_Rect * src, int x, int y):m_render(render), m_sprite(sprite),m_SpriteRect(*src)
{
	renderFlip = SDL_FLIP_VERTICAL;
	m_bound.x = static_cast<float>(src->w / 2);
	m_bound.y = static_cast<float>(src->h / 2);
	m_bound.z = static_cast<float>(-src->w / 2);
	m_bound.w = static_cast<float>(-src->h / 2);
	m_rect = { x, y, src->w, src->h };
	m_center = { src->w / 2, src->h / 2 };

	m_data.pSprite = m_sprite;
	m_data.spriteRect = m_SpriteRect;
	m_data.postionRect = m_rect;
	m_data.size = m_size;//��������
	m_data.angle = m_angle;
	m_data.pointFlip = m_center;
}

Sprite::Sprite(SDL_Texture * sprite, SDL_Rect * src, int x, int y): m_SpriteRect(*src), m_sprite(sprite)
{
	renderFlip = SDL_FLIP_VERTICAL;
	m_bound.x = static_cast<float>(src->w / 2);
	m_bound.y = static_cast<float>(src->h / 2);
	m_bound.z = static_cast<float>(-src->w / 2);
	m_bound.w = static_cast<float>(-src->h / 2);
	m_rect = { x, y, src->w, src->h };
	m_SpriteRect = {src->x, src->y, src->w, src->h };
	m_center = { src->w / 2, src->h / 2 };


	m_data.pSprite = m_sprite;
	m_data.spriteRect = m_SpriteRect;
	m_data.postionRect = m_rect;
	m_data.size = m_size;//�������� ��� �� ������
	m_data.angle = m_angle;
	m_data.pointFlip = m_center;
}

Sprite::~Sprite() {
	SDL_DestroyTexture(m_sprite);
}

void Sprite::Draw()
{
	//m_center = { (int)m_position.x + m_rect.w / 2, (int)m_position.y + m_rect.h / 2 };
	SDL_RenderCopyEx(m_render, m_sprite, nullptr, &m_rect, -m_angle, &m_center, renderFlip);
}

void Sprite::DrawTexture()
{
	
	//m_center = { (int)m_position.x + m_rect.w / 2, (int)m_position.y + m_rect.h / 2 };
	SDL_RenderCopyEx(m_render, m_sprite, &m_SpriteRect, &m_rect, -m_angle, &m_center, renderFlip);
}
			
//���������� ����� �������
 SDL_Point Sprite::getCenter()
{	
	m_center.x = m_rect.w / 2;
 	m_center.y = m_rect.h / 2; 
	return m_center; 
}


 Vec4	Sprite::getRect(int low)
{
	m_bound.x = m_position.x - (m_rect.w / 2 - low);
	m_bound.y = m_position.y - (m_rect.h / 2 - low);
	m_bound.z = m_position.x + (m_rect.w / 2 - low);
	m_bound.w = m_position.y + (m_rect.h / 2 - low);

	return m_bound;
}

 SDL_Rect& Sprite::getRect()
{
	return m_rect;
}


//�������� ���� ��� �������� ������
 void Sprite::setAngle(double ang) 
{ 
	m_angle = ang; 
}
//�������� �������
 void Sprite::setPosition(Vec2 _position) { //m_position = _position;

	m_position.x = _position.x;
	m_position.y = _position.y;
	m_rect.x = static_cast<int>(m_position.x);
	m_rect.y = static_cast<int>(m_position.y);
}
//���������� ������
 SDL_Texture* Sprite::getSprite() 
{
	return m_sprite; 
}

 SpriteData* Sprite::getSpriteData()
{
	m_data.pSprite = m_sprite;
	m_data.spriteRect = m_SpriteRect;
	m_data.postionRect = m_rect;
	m_data.size = m_size;//��������
	m_data.angle = m_angle;
	m_data.pointFlip = m_center;
	return &m_data;
}