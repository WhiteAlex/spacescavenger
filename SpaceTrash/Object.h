#pragma once
#define _CRT_SECURE_NO_WARNINGS

#ifndef OBJECT_H_INCLUDED
#define OBJECT_H_INCLUDED
#include <SDL2\SDL.h>
#include <iostream> 

#include <string>
#include <list>

#include "math.h"
#include "Image.h"					
 	
struct SpriteData
{
	SDL_Texture*  pSprite;		//������ 
	SDL_Rect	  spriteRect;	//������� �������
	SDL_Rect	  positionRect; //������� 
	SDL_Point	  pointFlip;	//����� ��������
	double		  size;			//��������� ��� ��������� �������
	double		  angle;		//���� ��������
};

class Object
	{
	enum STATE
	{
		DEAD = 0,

	};

	public:
		Object(Vec2 _xy);
		Object(Vec2 _xy, float zs);
		Object(Vec2 _xy, float zs, float srotate);
		virtual ~Object();
		
		//�������� �������
		virtual void Move() = 0;
		//���������� ������� �������
		virtual Vec2 GetPosition();
		//���������� ���� �������
		double GetAngle() { return m_spriteDate.angle; }
		//������ ���������� �������
		virtual void SetPosition(const Vec2& _position);
		//���������� ����������
		virtual void Update(float t) = 0;
		void SetSpriteDate(SDL_Texture* _texture, int _spriteX, int _spriteY, int _spriteWidth, int _spriteHeight);
	//	virtual void Transfer(Vec2 _transfer) = 0;
	//	SpriteData m_spriteDate;
	
		SpriteData* GetSpriteData() { return &m_spriteDate; }
		void SetSpriteDate(SDL_Texture* texture, SDL_Rect tile, double size = 1.0, double angle = 0.0);
	protected:
		Vec2 m_position;//��������� ����� ������ �� � � �� �
		SpriteData m_spriteDate;
	private:
		Object(const Object& object);

};
#endif //OBJECT_H_INCLUDE