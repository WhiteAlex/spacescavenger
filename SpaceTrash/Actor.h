#pragma once
//#include"Bot.h"
//#include"Asteroid.h"
//#include"Ship.h"
//#include"Weapon.h"
//#include"Sprite.h"

#include<iostream>

template<class T>
class Actor
{
public:
	//��� �������� ������� �� ���������� �����
	Actor(SDL_Renderer* ren, const std::string& fileName, SDL_Rect* rect, T* pBody) :m_texture(nullptr), m_face(nullptr), m_body(pBody)
	{
		int width = rect->w, height = rect->h;
		m_face = new Sprite(ren, fileName.c_str(), static_cast<int>(m_body->getPosition().x),
												  static_cast<int>(m_body->getPosition().y),
																							width, height);
	}
	//��� ��������
	Actor(SDL_Renderer* ren, const std::string& fileName, int posX, int posY, int width, int height)
	{
		m_face = new Sprite(ren, fileName.c_str(), posX, posY, width, height);
  
	}
	//��� �������� �� ������
	Actor(SDL_Texture* texture, T* pBody, int frameX, int frameY, int frameWidth, int frameHeight): m_body(pBody)
	{
		SDL_Rect temp = { frameX, frameY, frameWidth, frameHeight};
		m_face = new Sprite(texture,&temp, static_cast<int>(m_body->getPosition().x), static_cast<int>(m_body->getPosition().y));
	}

	inline Sprite* GetFace() { return m_face; }
	void Update(float time) 
	{
		m_body->Update(time);
		m_body->setRect(m_face->getRect());
		m_face->setPosition(m_body->getPosition());
		m_face->setAngle(m_body->getAngle());
	}
	inline T* GetBody() { return m_body; }
	~Actor()
	{
		delete m_face;
		delete m_body;
		SDL_DestroyTexture(m_texture);
		m_texture = nullptr;
	}

private:
//	Sprite* m_face;
	T* m_body;
	SDL_Texture* m_texture;
};

