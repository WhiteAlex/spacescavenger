#include "Object.h"

Object::Object(Vec2 _xy):m_position(_xy)
{
	m_spriteDate.pSprite = nullptr;
	m_spriteDate.positionRect.x = static_cast<int>(m_position.x);
	m_spriteDate.positionRect.y = static_cast<int>(m_position.y);
	m_spriteDate.size = 1.0;
	m_spriteDate.angle = 0.0;
}

Object::Object(Vec2 _xy, float zs):m_position(_xy)
{
	m_spriteDate.pSprite = nullptr;
	m_spriteDate.positionRect.x = static_cast<int>(_xy.x);
	m_spriteDate.positionRect.y = static_cast<int>(_xy.y);
	m_spriteDate.size = zs;
	m_spriteDate.angle = 0.0;
}

Object::Object(Vec2 _xy, float zs, float srotate):m_position(_xy)
{
	m_spriteDate.pSprite = nullptr;
	m_spriteDate.positionRect.x = static_cast<int>(_xy.x);
	m_spriteDate.positionRect.y = static_cast<int>(_xy.y);
	m_spriteDate.size = zs;
	m_spriteDate.angle = srotate;
}

Object::~Object()
{
//	SDL_DestroyTexture(m_spriteDate.pSprite);
	//delete m_spriteDate;
}

void Object::SetSpriteDate(SDL_Texture * _texture, int _spriteX, int _spriteY, int _spriteWidth, int _spriteHeight)
{
	m_spriteDate.pSprite	  = _texture;
	m_spriteDate.spriteRect.x = _spriteX;
	m_spriteDate.spriteRect.y = _spriteY;
	m_spriteDate.spriteRect.w = static_cast<int> (_spriteWidth  * m_spriteDate.size);
	m_spriteDate.spriteRect.h = static_cast<int> (_spriteHeight * m_spriteDate.size);
	m_spriteDate.pointFlip.x  = static_cast<int> (_spriteWidth  * m_spriteDate.size / 2);
	m_spriteDate.pointFlip.y  = static_cast<int> (_spriteHeight * m_spriteDate.size / 2);

}

void Object::SetSpriteDate(SDL_Texture * texture, SDL_Rect tile, double size, double angle)
{
	m_spriteDate.pSprite = texture;
	m_spriteDate.spriteRect = tile;
	m_spriteDate.positionRect = { static_cast<int> (m_position.x), static_cast<int> (m_position.y), tile.w, tile.h};
	m_spriteDate.pointFlip = { tile.w / 2, tile.h / 2 };
	m_spriteDate.size = size;
	m_spriteDate.angle = angle;

}

Vec2 Object::GetPosition()
{
	return m_position;
}

void Object::SetPosition(const Vec2& _position)
{
	m_position = _position;
}