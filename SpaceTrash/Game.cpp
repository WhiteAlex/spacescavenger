#define _CRT_SECURE_NO_WARNINGS
#include "Game.h"

//mainCRTStartup
Game::Game():screen_width(1024),screen_height(768), isRun(true)
{
	m_time.factor = 0.01f;
	m_time.acceleration = 3.5f;
	m_time.PrevTime = 0.f;
	joystick = SDL_JoystickOpen(0);
//	fps.framePerSecond = 0.0f;
//	fps.lastTime = 0.0f;
	
	//�������� ����
	m_display = new Display("Space scavenger", screen_width, screen_height);
	//�������� ���������� ������� �����.
	m_sound = new Sound;
	//�������� �����
	m_scene = new Scene(m_sound,"res/sprite/bg.png",m_display->getRender());
	
	assets = SDL_CreateTextureFromSurface(m_display->getRender(),
								Image::LoadImage("res/sprite/My.png", 255, 170, 255));
	
	m_scene->SetAsset(assets);
	SDL_Color colorText = { 255,255 ,255,255 };
	text = new Text(m_display->getRender(), "res/ttf/MarckScript-Regular.ttf", colorText, 24);
	//text1 = new Text(m_display->getRender(), "res/ttf/MarckScript-Regular.ttf", colorText, 24);


	pship = new CShip(Vec2(static_cast<float>(PCT(screen_width, 50)),
					       static_cast<float>(PCT(screen_height, 50))));
	pship->SetSpriteDate(assets, { 15,0,64,100 });

	cur = Image::LoadImageAsSurface("res/sprite/metka1.png", 0, 0, 24, 24);
	SDL_Cursor* cursor = SDL_CreateColorCursor(cur, 12, 12);
	SDL_SetCursor(cursor);

	
}
bool Game::isExecute()
{	

	while (isRun)
	{
		SDL_PumpEvents();
		//��������� ���� �������� �������
			while (SDL_PollEvent(&sdl_event)!=0)
				Game::isEvent(&sdl_event);
			//��������� ������� ������ ������ �����
			if (SDL_GetMouseState(NULL, NULL)&SDL_BUTTON(SDL_BUTTON_LEFT))
			{
				//�������� ������� � ������ �����
				if (!pship->getShot())
				{
					m_sound->PlayEffect();
					m_scene->setBullet(pship->Shot());
				}
			}
			//��������� ������� ��������� ������
			keystate = SDL_GetKeyboardState(NULL);
			//���������� �������� ������ �� ������� WASD
			if (keystate[SDL_GetScancodeFromKey(SDLK_w)]) pship->Transfer(Vec2(0, -1));
			if (keystate[SDL_GetScancodeFromKey(SDLK_a)]) pship->Transfer(Vec2(-1, 0));
			if (keystate[SDL_GetScancodeFromKey(SDLK_s)]) pship->Transfer(Vec2(0, 1));
			if (keystate[SDL_GetScancodeFromKey(SDLK_d)]) pship->Transfer(Vec2(1, 0));


			//---------------------------------------------------------------------------
			////�������� ����� � ���������� ��� ������� ������ f1 � f2
			if (keystate[SDL_GetScancodeFromKey(SDLK_F1)]) m_scene->CreateEnvironment(BOT,assets);
			if (keystate[SDL_GetScancodeFromKey(SDLK_F2)]) m_scene->CreateEnvironment(ASREROID, assets);
			//�������� ����� � ���������� ��� ������� ������ f3 � f4
			if (keystate[SDL_GetScancodeFromKey(SDLK_F3)]) m_scene->isDestroy(BOT);
			if (keystate[SDL_GetScancodeFromKey(SDLK_F4)]) m_scene->isDestroy(ASREROID);
			//---------------------------------------------------------------------------

			m_time.CurrentTime = (float)SDL_GetTicks();
			m_time.DeltaTIme = m_time.CurrentTime - m_time.PrevTime;
			time = m_time.DeltaTIme* m_time.acceleration*m_time.factor;
			
			//������
			isLoop();
			//��������� 
			Render(); //draw	
			GUI_Render();

			m_time.PrevTime = m_time.CurrentTime;
			 //�������� ����� �� �� ������������ ��������(������� ����� ������� < 0)
			m_scene->isClearScene();
	}
	//������������ ������
	Cleanup();

	return isRun;
}
void Game::GUI_Render()
{
	text->ShowText(PCT(screen_width, 0), PCT(screen_height, 105), "FPS: %d  x: %i, y: %i",
		static_cast<int>(fps.FPS), static_cast<int>(mouse.x), static_cast<int>(mouse.y));
}
int Game::SetCursorPosition(float x, float y)
{
	mouse = Vec2(x, y);
	return 1;
}

void Game::Render()
{	
	//���������� FPS
	fps.currentTime = (float)SDL_GetTicks()*0.001f;
	++fps.framePerSecond;

	if (fps.currentTime - fps.lastTime > 1.0f)
	{
		fps.lastTime = fps.currentTime;
		fps.FPS = fps.framePerSecond;
		fps.framePerSecond = 0;
	} 
	//������� ������
		m_display->isClear();
	 //��������� �����
		m_scene->isRender(m_display);
	//���������� ������
		m_display->Render(pship);
		
		

	m_display->RenderPresent();
}
void Game::Cleanup()
{
	SDL_FreeCursor(cursor);
	SDL_FreeSurface(cur);
	delete text;

//	delete m_scene;
//		m_scene = nullptr;

//	delete pship;
	//	pship = nullptr;

//	delete m_sound;
//		m_sound= nullptr;

	delete m_display;
		m_display = nullptr; 
	SDL_JoystickClose(joystick);
	TTF_Quit();
	IMG_Quit();
	Mix_Quit();
	SDL_Quit();
}
Game::~Game()
{
	SDL_DestroyTexture(assets);
//	SDL_DestroyTexture(texture);
	assets = nullptr;
//	texture = nullptr;

	delete text;
	text = nullptr;

	//TTF_CloseFont(TTFtext);

	delete m_scene;
	m_scene = nullptr;

	delete pship;
	pship = nullptr;

	delete m_sound;
	m_sound = nullptr;

	delete m_display;
	m_display = nullptr;

	SDL_Quit();
}
void Game::isLoop()
{
		pship->Update(time);
		pship->SetTargetXY(mouse);
		m_scene->isLoop(time, pship);
}
void Game::isEvent(SDL_Event* e)
{
	m_display->Event(e);
	if ((e->type == SDL_QUIT))	isRun = false;
	   //��������� ��������� ������� ����
	if (e->type == SDL_MOUSEMOTION) 
	{
		SetCursorPosition(static_cast<float>(e->motion.x),
							static_cast<float>(e->motion.y));

	//	textRect.x = e->motion.x-textRect.w/2; textRect.y = e->motion.y;
		//int x, y;
	//	SDL_GetMouseState(&x, &y);
	//	SetCursorPosition(x+47,y+50);

	}

	SDL_JoystickUpdate();
	if (e->type == SDL_JOYAXISMOTION)
	{
		auto x = SDL_JoystickGetAxis(joystick, 0);
		auto y = SDL_JoystickGetAxis(joystick, 1);
		pship->Transfer(Vec2(static_cast<float>(x) / 32767, static_cast<float>(y) / 32767));
	}
}