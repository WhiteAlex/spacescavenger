#include"math.h"

VERTEX2D_TYP::VERTEX2D_TYP()
{
	x = 0.0f; 	y = 0.0f; 	z = 0.0f; w = 0.0f;
}
VERTEX2D_TYP::VERTEX2D_TYP(float _x, float _y) {
	x = _x; y = _y; z = 0.0f; w = 0.0f;
}
VERTEX2D_TYP::VERTEX2D_TYP(float _x, float _y, float _z) {

	x = _x; y = _y; z = _z;
}
VERTEX2D_TYP::VERTEX2D_TYP(float _x, float _y, float _z, float _w) {

	x = _x; y = _y; z = _z; w = _w;
}

//VERTEX2D_TYP VERTEX2D_TYP::Turn(Vec2 coords, double angle)
//{
//	double ang = angle * PI / 180;
//	double xr = 0, yr = 0;
//
//	xr = coords.x * cos(-ang) - coords.y*sin(-ang);
//	yr = coords.x * sin(-ang) + coords.y*cos(-ang);
//	return VERTEX2D_TYP(static_cast<float>(xr), static_cast<float>(yr));
//}

double GetDist(Vec2 point, Vec2 point2)
{
	return sqrt((point.x - point2.x)*(point.x - point2.x) + (point.y - point2.y)*(point.y - point2.y));
}

//VERTEX2D_TYP Turn(Vec2 coords, double angle)
//{
//	double ang = angle * PI / 180;
//	double xr = 0, yr = 0;
//
//	xr = coords.x * cos(-ang) - coords.y*sin(-ang);
//	yr = coords.x * sin(-ang) + coords.y*cos(-ang);
//	return VERTEX2D_TYP(static_cast<float>(xr), static_cast<float>(yr));
//}