#include "Weapon.h"

Bullet::Bullet(Vec2 _xy,Vec2 _target, float dmg):
        Entity(_xy,1), damage(dmg)
{
	m_target = _target;
	m_acceleration = 10.0f;

	float xv = m_target.x - m_position.x;
	float yv = m_target.y - m_position.y;

	float dist = sqrt(xv*xv + yv*yv);
	m_velocity = Vec2(xv / dist * 2, yv / dist * 2);
}

Bullet::~Bullet()
{

}

void Bullet::Move()
{
	m_position.x += m_acceleration * (m_velocity.x) * time;
	m_position.y += m_acceleration * (m_velocity.y) * time;
	/*bound.x = position.x - bound.w*0.5;
	bound.y = position.y - bound.w*0.5;*/

}
float Bullet::getDamage()
{
	return this->damage;
}

//void Bullet::Update(float t)
//{
//	m_spriteDate.positionRect.x = m_position.x;
//	m_spriteDate.positionRect.y = m_position.y;
//
//}