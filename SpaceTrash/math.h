#ifndef MATH_H
#define MATH_H
#include<math.h>
#define PCT(x,y) x / (x  / (x / 100))*y
const double PI = 3.14159265358979323846;
	typedef struct VERTEX2D_TYP
	{
	public:
		float x;
		float y;
		float z;
		float w;

		VERTEX2D_TYP();
		VERTEX2D_TYP(float _x, float _y);
		VERTEX2D_TYP(float _x, float _y, float _z);
		VERTEX2D_TYP(float _x, float _y, float _z, float _w);
		//VERTEX2D_TYP Turn(Vec2 coords, double angle);
		void operator=(const VERTEX2D_TYP& _xy)
		{
			x = _xy.x;
			y = _xy.y;
			z = _xy.z;
			w = _xy.w;
		}

		friend VERTEX2D_TYP operator+(VERTEX2D_TYP a, VERTEX2D_TYP b)
		{
		/*	VERTEX2D_TYP c;

			c.x = a.x + b.x;
			c.y = a.y + b.y;
			c.z = a.z + b.z;
			c.w = a.w + b.z;*/
			return VERTEX2D(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);;
		}

		friend VERTEX2D_TYP operator-(VERTEX2D_TYP a, VERTEX2D_TYP b) {
			/*VERTEX2D_TYP c;
			c.x = a.x - b.x;
			c.y = a.y - b.y;
			c.z = a.z - b.z;
			c.w = a.w - b.z;*/
			return VERTEX2D(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
		}
		friend VERTEX2D_TYP operator*(VERTEX2D_TYP a, float b)
		{
			/*VERTEX2D_TYP c;
			c.x = a.x * b;
			c.y = a.y * b;
			c.z = a.z * b;
			c.w = a.w * b;*/
			return VERTEX2D(a.x * b, a.y * b, a.z * b, a.w * b);
		}


	
	}CVECTOR, VERTEX2D, *VERTEX2D_PTR, Vec2, Vec3, Vec4;



#endif // !MATH_H


#ifndef _RECTBOUND_
#define _RECTBOUND_

typedef struct RECT_TYP {
	int maxX;
	int maxY;
	int minX;
	int minY;
}RectBound;

#endif // !RECTBOUND

#ifndef _HP_
#define _HP_

typedef struct HP_TYP {
	float pctHP; //������� ��������
	float currentPctHP;// ������� ������� ��������
	float currentHP;// ������� ��������
	float maxHP;	// ������ ��������
	bool  bLife;// ����� ���, �� ����� �� ���
}HP;

#endif // !_HP_

