#include "Asteroid.h"


Asteroid::Asteroid(Vec2 _xy, float zsize, float srotate):
Entity(_xy, zsize,srotate),velocityRotate(srotate), ang(0)
{
	//    ang = angle * 180 / PI;
	
}

void Asteroid::Update(float t)
{
	time = t;

	ang += velocityRotate;
	m_pctHP = m_maxHP / 100;
	m_currentPctHP = m_currentHP / m_pctHP;
	m_spriteDate.positionRect.x = static_cast<int>(m_position.x);
	m_spriteDate.positionRect.y = static_cast<int>(m_position.y);
	m_spriteDate.angle = ang;
}

Asteroid::~Asteroid()
{
	//std::cout << "\nAsteroid destroy";
}

//std::list<Asteroid*> Asteroid::Death()
//{
//	std::list<Asteroid*> asteroid;
//	for (int index = 0; index < size-1; ++index)
//	{	
//		asteroid.push_back(new Asteroid( position, 1 / scale, 1 - rand() % 3));
//		std::cout << "\nCreate Asteroide";
//	}
//	return asteroid;
//}