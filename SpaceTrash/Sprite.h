#ifndef SPRITE_H_
#define SPRITE_H_

#include<SDL2\SDL.h>
#include<string>
#include"Image.h"
#include"math.h"

//struct SpriteData
//{
//	SDL_Texture*  pSprite;
//	SDL_Rect	  spriteRect;
//	SDL_Rect	  postionRect;
//	SDL_Point	  pointFlip;
//	double		  size;
//	double		  angle;
//};


class Sprite
{

public:
	Sprite(SDL_Renderer* render, const std::string& fileName, int x, int y, int width, int height);
	Sprite(SDL_Renderer* render, SDL_Texture* sprite, SDL_Rect* src, int x, int y);
	Sprite(SDL_Texture* sprite, SDL_Rect* src,int x, int y);
	~Sprite();

	//���������� ����� �������
	 SDL_Point Sprite::getCenter();
	//���������� ����� ��� ����������� ������������
	//low ���������� �����, �� ��������� ����� 0
	 Vec4	Sprite::getRect(int low);
	SDL_Rect& getRect();
	//�������� ���� ��� �������� ������
	 void setAngle(double ang);
	//�������� �������
	 void setPosition(Vec2 _position);
	//���������� ������
	 SDL_Texture* getSprite();
	 SpriteData* getSpriteData();
	//���������
	void Draw();
	void DrawTexture();

private:
	SDL_Texture* m_sprite;
	SDL_Rect m_SpriteRect;
	SDL_Rect m_rect;
	SDL_Point m_center;
	SDL_Renderer* m_render;
	Vec2 m_position;
	Vec4 m_bound;
	SpriteData m_data;
	SDL_RendererFlip renderFlip;
	double m_size;
	double m_angle;
};
#endif // !SPRITE_H_



