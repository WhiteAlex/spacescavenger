#pragma once
#include"Entity.h"

class Asteroid:
	public Entity
{

public:
	Asteroid(Vec2 _xy, float zsize, float srotate);
	void Update(float t);
	double getAngle() { return ang; }
	//Vec2 getPosition() { return position; }
	//std::list<Asteroid*> Death();
	~Asteroid();


  private:
	  Asteroid(const Asteroid& asteroid);
	float velocityRotate;
	double ang;

};

