#pragma once
#include<SDL2\SDL.h>
#include<string>
#include"Image.h"
class button
{

	SDL_Renderer* m_render;
	SDL_Rect m_rect;
	SDL_Texture* backgroundButton;
	std::string caption;
	button(SDL_Renderer* _render, const std::string& file, const SDL_Rect _rect, const std::string& text);
public:
	button* Create(SDL_Renderer* _render, const std::string& file, 
					const SDL_Rect _rect, const std::string& text);
	void Show();
	virtual~button();
};

