#include "Weapon.h"
#include<iostream>

Weapon::Weapon(Vec2 _xy, int count, float dmg):countBullet(1),damage(1)
{
	owner = _xy;
	countBullet = count;
	damage = dmg;				
	
} 
Weapon::Weapon(Vec2 _xy, Vec2 _target)
{
}  
void Weapon::setXY(Vec2 _xy)
{
	owner = _xy;
}
void Weapon::setTarget(Vec2 _xy)
{ 
}

void Weapon::Update()
{
	m_timer.Update();
}

std::list<Bullet*> Weapon::Shot(Vec2& _tar)
{

	SDL_Rect bound = { (int)owner.x, (int)owner.y, 10, 50 };
	ammo--;
	std::list<Bullet*> bullets;

	if (m_timer.CheckCooldown(1500))
		 bullets.push_back(new Bullet(owner, _tar, damage));
	
	return bullets;
}
int Weapon::getAmmo()
{
	return ammo;
}
void Weapon::setAmmo(int am)
{
	ammo = am;
}
Weapon::~Weapon()
{
}

